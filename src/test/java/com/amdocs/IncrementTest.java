package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {

        public IncrementTest() {
                // TODO Auto-generated constructor stub
        }

       @Test
        public void getCounter() throws Exception {

                final int first= new Increment().getCounter();
                assertEquals("Result", 1, first);

        }

        @Test
        public void decreasecounterWith0() throws Exception {

                final int second= new Increment().decreasecounter(0);
                assertEquals("Result", 0, second);

        }

         @Test
        public void decreasecounterWith2() throws Exception {

                final int thirdCase= new Increment().decreasecounter(3);
                assertEquals("Result", 2, thirdCase);

        }

         @Test
        public void decreasecounterWith1() throws Exception {

                final int fourthCase= new Increment().decreasecounter(1);
                assertEquals("Result", 1, fourthCase);

        }
}
